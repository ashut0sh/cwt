#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "utils.h"

// Utility to translate between scale and frequency for a specified CWT file.
// The frequencies or scales should be specified as a comma separated list with
// no spaces.

void print_usage_exit()
{
    fprintf(stderr, "USAGE: scutil  -i     <input CWT file>\n"
                    "               -f/-s  <comma separated list of input scales or frequencies to translate; use only one>\n");
    exit(1);
}

int main(int argc, char **argv)
{
    int opt = 0, read = 0, Fset = 0, Sset = 0;
    char *ifile = NULL, *f = NULL, *s = NULL, *temp = NULL;
    const char  waveletName[2][16] = {"Morlet", "Mexican Hat"};

    psrCWT pcwt = {NULL, 0.0, 0.0, 0, 0, 0, 0, 0.0, 0.0};

    if (argc < 4)
        print_usage_exit();

    extern char *optarg;
    while ((opt = getopt(argc, argv, "i:f:s:")) != -1)
    {
        switch (opt)
        {
            case 'i':
                read = 1;
                ifile = strdup(optarg);
                break;
            case 'f':
                Fset = 1;
                f = strdup(optarg);
                break;
            case 's':
                Sset = 1;
                s = strdup(optarg);
                break;
            default:
                print_usage_exit();
                break;
        }
    }

    if (argc != 5 || (Fset && Sset) || !read)
    {
        fprintf(stderr, "Illegal arguments: Please specify an input \
                         CWT file and a frequency OR a scale.");
        exit(1);
    }

    if ((readCWT(ifile, &pcwt)) != 0)
    {
        fprintf(stderr, "Failed to read from input CWT file: %s.\n", ifile);
        free(ifile);
        free(s ? s : f);
        free(pcwt.spec);
        exit(EXIT_FAILURE);
    }

    printf("N = %d\tJ = %d\tT = %0.2f s\n", pcwt.N, pcwt.J, pcwt.T);
    printf("Wavelet: %s.\n", waveletName[pcwt.W]);
    printf("Highest frequency: %0.4f Hz\t Lowest Frequency: %0.4f Hz\n", 
            stof(pcwt.w0, pcwt.N, pcwt.T, 0.1), stof(pcwt.w0, pcwt.N, pcwt.T, pcwt.J));
    
    if (Fset)
    {
        temp = strtok(f, ",");
        printf("Freq (Hz)\tScale\n");
        do {
            printf("%0.4f\t\t%0.0f\n", atof(temp), 
                    ftos(pcwt.w0, pcwt.N, pcwt.T, atof(temp)));
        } while ((temp = strtok(NULL, ",")) != NULL);
        free(f);
    }
    else
    {
        temp = strtok(s, ",");
        printf("Scale\t\tFreq (Hz)\n");
        do {
            printf("%0.0f\t\t%0.4f\n", atof(temp), 
                    stof(pcwt.w0, pcwt.N, pcwt.T, atof(temp)));
        } while ((temp = strtok(NULL, ",")) != NULL);
        free(s);
    }

    free(pcwt.spec);
    free(ifile);

    return 0;
}

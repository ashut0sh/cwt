CC	    =  gcc
CFLAGS +=  -std=gnu99 -Wall -Werror
LIBS   +=  -lfftw3_threads -lfftw3 -lcpgplot -lpgplot -lgfortran -lpthread 
LIBS   +=  -lrt -lgsl -lgslcblas -lmpfit -lm

default: all

cwt1: main1.c cwt.o utils.o
	$(CC) $(CFLAGS) $(EFLAGS) -o cwt1       main1.c        cwt.o utils.o $(LIBS)

cwt2: main2.c cwt.o utils.o
	$(CC) $(CFLAGS) $(EFLAGS) -o cwt2       main2.c        cwt.o utils.o $(LIBS)

wavreader: wavreader.c utils.o
	$(CC) $(CFLAGS) $(EFLAGS) -o wavreader  wavreader.c          utils.o $(LIBS)

scutil: scutil.c utils.o
	$(CC) $(CFLAGS) $(EFLAGS) -o scutil     scutil.c             utils.o $(LIBS)

maskviewer: maskviewer.c
	$(CC) $(CFLAGS) $(EFLAGS) -o maskviewer maskviewer.c		 utils.o $(LIBS)

distslp: distslp.c
	$(CC) $(CFLAGS) $(EFLAGS) -o distslp 	distslp.c			 		 $(LIBS)

fitgauss: fitgauss.c
	$(CC) $(CFLAGS) $(EFLAGS) -o fitgauss   fitgauss.c                   $(LIBS)

%.o: %.c
	$(CC) -c $(CFLAGS) $(EFLAGS) $< -o $@

debug: main1.c main2.c wavreader.c scutil.c integrate.c
	$(CC) -c $(CFLAGS)  -ggdb  -o cwt.o         cwt.c                         $(LIBS)
	$(CC) -c $(CFLAGS)  -ggdb  -o utils.o       utils.c                       $(LIBS)
	$(CC)    $(CFLAGS)  -ggdb  -o cwt1 			main1.c 	 cwt.o utils.o	  $(LIBS)
	$(CC)    $(CFLAGS)  -ggdb  -o cwt2          main2.c      cwt.o utils.o    $(LIBS)
	$(CC)    $(CFLAGS)  -ggdb  -o wavreader		wavreader.c 	   utils.o	  $(LIBS)
	$(CC)    $(CFLAGS)  -ggdb  -o scutil        scutil.c    	   utils.o	  $(LIBS)
	$(CC)	 $(CFLAGS)  -ggdb  -o maskviewer    maskviewer.c       utils.o	  $(LIBS)
	$(CC)    $(CFLAGS)  -ggdb  -o distslp       distslp.c                     $(LIBS)
	$(CC)    $(CFLAGS)  -ggdb  -o fitgauss      fitgauss.c                    $(LIBS)

all: cwt1 cwt2  wavreader scutil maskviewer distslp fitgauss

clean:
	rm -rf wavreader cwt1 cwt2 scutil maskviewer distslp fitgauss gmon.out log* *.o 

dataclean:
	rm -rf *.png *.ps *.cwt *.csv *.dat

cleaner: clean dataclean

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>
#include <cpgplot.h>
#include <gsl/gsl_cdf.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_statistics_float.h>

#include "cwt.h"
#include "utils.h"

int* getDriftBands(psrMWT p, const float pval, const int s, 
                   int *px, int *py, size_t *Npts)
{
    int *dbImg = calloc(p.bw * p.N, sizeof(int));
    float *sigl  = calloc(p.bw,     sizeof(float));
    float *amplt = &p.mwt[s][0], *phase = &p.mwt[s][p.bw * p.N];
    float mn = 0.0, wd = 0.5;

    for (int i = 0; i < p.bw; i++)
        sigl[i] = gsl_cdf_chisq_Qinv(1 - pval, 2.0) 
            * gsl_stats_float_variance(&amplt[i], p.bw, p.N);
    
    for (int i = 0; i < p.bw * p.N; i++)
            amplt[i] *= (amplt[i] / sigl[i % p.bw]);
   
    int count = 0;
    for (int i = 0; i < p.N; i++)
    {
        for (int j = 0; j < p.bw; j++)
        {
            if (phase[i * p.bw + j] > mn - wd &&
                phase[i * p.bw + j] < mn + wd && 
                amplt[i * p.bw + j] > 1)
            {
                dbImg[i * p.bw + j] = 1;
                px[count] = (p.Nb - p.bw) / 2 + j;
                py[count] = i;
                count++;
            }
        }
    }

//    px = realloc(px, count * sizeof(int));
//    py = realloc(py, count * sizeof(int));

    *Npts = count;

    free(sigl);

    return dbImg;
}

int* getModeMap(double **partitions, const size_t M, const size_t N)
{
    int *mask = calloc(N, sizeof(int));

    for (int i = 0; i < M; i++)
        for (int j = 2; j < partitions[i][0]; j += 2)
            mask[(int) partitions[i][j]] = (partitions[i][0] / 2 > 4) ? 1 : 0;
    
    return mask;
}

int** cleanMasks(int **masks, const size_t M, const size_t N, int *psel)
{
    int flag;
    int **cmasks = malloc(M * sizeof(int*));

    for (int i = 0; i < M; i++)
        cmasks[i] = calloc(N, sizeof(int));
    
    for (int i = 0; i < N; i++)
    {
        flag = 0;
        for (int j = 0; j < M; j++)
        {
            for (int k = 0; k < M; k++)
                if (j != k && masks[j][i] && masks[k][i])
                    flag = 1;

            if (!flag && masks[j][i] == 1)
            {
                cmasks[j][i] = 1;
                psel[j]++;
            }
        }
    }

    return cmasks;
}

int plotIProf(float **iprofs, const int *scales, const int *nPi, const size_t M, 
              const size_t N, const char *label, const char *format_spec,
              const int *limits)
{
    float *xaxis = linspace_f(1.0, N, N), *lx, *ly;
    char title[64], buf[64];
    int lN;
    sprintf(title, "Integrated Profile: %s", label);

    float max = 0, min = 0;

    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
//            iprofs[i][j] /= (float) dby[i];

            if (iprofs[i][j] > max)
                max = iprofs[i][j];
            else if (iprofs[i][j] < min)
                min = iprofs[i][j];
        }
    }

    min -= 0.1 * min;
    max += 0.1 * max;

    if (cpgbeg(0, format_spec, 1, 1) != 1)
        return 2;

    cpgscr(0, 1, 1, 1);
    cpgscr(1, 0, 0, 0);
    cpgenv((float) limits[1], (float) limits[2], min, max, 0, 0);
    cpgsch(2.0);
    cpgmtxt("T", 1.0, 0.5, 0.5, title);
    cpgsch(1.5);
    cpgmtxt("RV", -8.0, 1 - 0.048, 0.5, "Legend: Scales (Periods)");
    cpgmtxt("L", 1.5, 0.5, 0.5, "Bins");
    cpgmtxt("B", 1.5, 0.5, 0.5, "Counts");

    for (int i = 0; i < M; i++)
    {
        sprintf(buf, "%d   (%04d)", scales[i + 1], nPi[i]);
        lN = (int) ceilf(N / 20);
        lx = linspace_f(limits[1] + 0.58 * (limits[2] - limits[1]), 
                        limits[1] + 0.62 * (limits[2] - limits[1]), lN);
        ly = calloc(lN, sizeof(float));
        for (int j = 0; j < lN; j++)
            ly[j] = max * (1 - 0.05 * (i + 2));
        cpgsci(i + 2);
        cpgline(limits[2] - limits[1], &xaxis[limits[1]], &iprofs[i][limits[1]]);
        cpgline(lN, lx, ly);
        cpgsci(1);
        cpgmtxt("RV", -5.5, 1 - 0.048 * (i + 2), 0.5, buf); 
        free(lx);
        free(ly);
    }

    cpgend();

    free(xaxis);

    return 0;
}

void print_usage_exit(void)
{
    fprintf(stderr, "USAGE: cwt2    -D <input data file>\n"
                    "               -J <highest scale>\n"
                    "               -K <initial scale>\n"
                    "               -W <0 for Morlet, 1 for Mexican hat; default=0>\n"
                    "               -w <order/centre frequency of the wavelet; default=2pi>\n"
                    "               -b <hyphen separated range of bins to use (eg. 30-70)>\n"
                    "               -O <prefix for output (MWT file, mode-separated DAT files) and plot title.>\n"
                    "               -s <comma separated list of scales to use as modes (eg. 20,28,30)>\n"
                    "               -L <title of the integrated plot>\n"
                    "               -H <number of angles in Hough transform; default=1800>\n"
                    "               -l <hyphen separated range of periods to use, fixes to default if invalid>\n"
                    "               -M <previously generated mwt file; data file, scales, etc are required>\n"
                    "               -m <comma separated list of drift mode slopes in ms/period (eg. -12,-8,-4)\n"
                    "                   should be specified for longitude-drifting pulsars>\n"
                    "               -p <significance level threshold for longitude drifting pulsars; default=0.687>\n"
                    "               -t <threshold factor for mode separation; default=2>\n");
    exit(1);
}
    
int main(int argc, char **argv)
{
    int N = 0, J = 0, J0 = 0, W = -1, *bins, *scales, mcount = 0, limits[3] = { 0 };
    int opt = 0, pulsardat = 0, mwfile = 0, write = 0, size = 0, scount = 0, bw = 0;
    int nAngles = 1800;
    float T = 0, *S = NULL, modes[16], w0 = 2 * pi, *pow, **powsums, thres_factor = 2;
    float pval = 0.687;
    char *dfile = NULL, *mfile = NULL, *opref = NULL, *tmp, buf[512], *ititle = NULL;

    bins   = calloc(3,  sizeof(int));
    scales = calloc(16, sizeof(int));

    fftw_complex *(*wavelets[2]) (const int N, const int J, const int J0, const float w0);
    wavelets[0] = morlet_fft;
    wavelets[1] = mexican_fft;

    if (argc < 4)
        print_usage_exit();

    psrDAT dat;
    psrMWT pmwt = {NULL, NULL, 0.0, 0, 0, NULL, NULL, 0, 0, 0, 0.0, NULL};

    while ((opt = getopt(argc, argv, "J:K:W:w:l:H:L:D:M:m:b:p:O:s:t:")) != -1)
    {
        switch (opt)
        {
            case 'J':
                J = atoi(optarg);
                break;

            case 'K':
                J0 = atoi(optarg);
                break;

            case 'W':
                W = atoi(optarg);
                break;

            case 'w':
                w0 = atof(optarg);
                break;

            case 'D':
                pulsardat = 1;
                dfile = strdup(optarg);
                break;

            case 'M':
                mwfile = 1;
                mfile = strdup(optarg);
                break;

            case 'L':
                ititle = strdup(optarg);
                break;

            case 'H':
                nAngles = atoi(optarg);
                break;

            case 'b':
                bins[0] = 1;
                bins[1] = atoi(strtok(optarg, "-"));
                bins[2] = atoi(strtok(NULL,   "-"));
                bw = bins[2] - bins[1];
                break;

            case 'l':
                limits[0] = 1;
                limits[1] = atoi(strtok(optarg, "-"));
                limits[2] = atoi(strtok(NULL,   "-"));
                break;

            case 'O':
                write = 1;
                opref = strdup(optarg);
                break;

            case 's':
                scount = 1;  
                scales[scount] = atoi(strtok(optarg, ","));
                while ((tmp = strtok(NULL, ",")) != NULL)
                    scales[++scount] = atoi(tmp);
                scales[0] = scount;
                free(tmp); // Check for leaks
                break;

            case 'm':
                mcount = 1;
                modes[mcount] = atoi(strtok(optarg, ","));
                while ((tmp = strtok(NULL, ",")) != NULL)
                    modes[++mcount] = atof(tmp);
                scales[0] = mcount;
                free(tmp);
                break;

            case 't':
                thres_factor = atof(optarg);
                break;

            case 'p':
                pval = atof(optarg);
                break;

            default:
                print_usage_exit();
                break;
        }
    }

    if (J0 > J || J0 < 0 || J <= 0)
    {
        fprintf(stderr, "Scale value error!");
        exit(1);
    }

    if (W < 0 || W > 1)
    {
        fprintf(stderr, "Invalid wavelet specified, using Morlet.");
        W = 0;
    }

    if (w0 == 2 * pi)
        fprintf(stderr, "Wavelet frequency/order not specified, using %.2f.\n", w0);

    if ((readDAT(dfile, &dat)) != 0)
    {
        fprintf(stderr, "Failed to read single pulse data file: %s.",
                        dfile);
        exit(1);
    }
    free(dfile);

    if (pulsardat && !mwfile)
    {
        N = dat.Nperiods; T = dat.T;

        fftw_complex *m = (*wavelets[W])(N, J, J0, w0);

        printf("Read the input file with %d periods and %d bins, of duration %f seconds.\n", 
                N, dat.Nbins, T);
        printf("Will perform wavelet transform from scale %d to %d for bins %d to %d.\n", J0, J, bins[1], bins[2]);
        printf("The following %d scales will be stored in the MWT file: ", scales[0]);
        for (int i = 1; i <= scales[0]; i++)
            printf("%d%s", scales[i], (i == scales[0]) ? ".\n" : ", ");

        float *spec, **mwt = malloc(scales[0] * sizeof(float*));
        size = bw * dat.Nperiods;
        for (int i = 0; i < scales[0]; i++)
            mwt[i] = calloc(size * 2 , sizeof(float));
        S = calloc(bw, sizeof(float));

        double *y   = calloc(N, sizeof(double));

        powsums = calloc(scales[0], sizeof(float*));
        for (int i = 0; i < scales[0]; i++)
            powsums[i] = calloc(dat.Nperiods, sizeof(float));

        for (int i = 1; i <= scales[0]; i++)
        {
            pow     = calloc(bw * dat.Nperiods, sizeof(float));
            for (int j = 0; j < bw; j++)
            {
                for (int k = 0; k < N; k++)
                    y[k] = dat.data[dat.Nbins * k + bins[1] + j];
               
                spec = cwt(y, m, N, J, J0, 0);

                S[j] = gsl_stats_sd(y, 1, N); 

                for (int k = 0; k < N; k++)
                {
                    mwt[i - 1][j + k * bw] 
                        = spec[(scales[i] - J0) * dat.Nperiods + k];

                    mwt[i - 1][j + (k + dat.Nperiods) * bw]
                        = spec[(J + scales[i] - 2 * J0) * dat.Nperiods + k];

                    for (int l = -1; l <= 1; l++)
                        pow[j * N + k] +=
                            SQR(spec[(scales[i] - J0 + l) * dat.Nperiods + k]);
                }
                
                free(spec);
            }
            
            for (int j = 0; j < bw; j++)
                for (int k = 0; k < dat.Nperiods; k++)
                    powsums[i - 1][k] += pow[j * N + k];
            
            free(pow);
        }
        
        pmwt = ((psrMWT) {mwt, powsums, dat.P, N, dat.Nbins, bins, scales, W, size, bw, T, S});

        if (write)
        {
            sprintf(buf, "%s.mwt", opref);
            if ((writeMWT(buf, pmwt)) != 0)
            {
                fprintf(stderr, "Failed to write to MWT file: %s.\n", opref);
                exit(1);
            }
        }
        free(y);
        fftw_free(m);
    }

    if (mwfile)
    {
        if ((readMWT(mfile, &pmwt)) != 0)
        {
            fprintf(stderr, "Failed to read from MWT file: %s.\n", mfile);
            exit(1);
        }
        free(mfile);
    }

    size_t Npts;
    int *px, *py, *dbImg, *ht1, *ind, Nind, angleNo, tmp2[2], nlines;
    int *psel, **masks, **cmask, *nPi, vc = 0;
    float theta, slope, *intercepts, tmp1[2], dist, **iprfs, **bstds, *vals;
    double **partitions, *lines, cov00, cov01, cov11, chisq;
    FILE *f, *slog, *mlog;

    masks = malloc(pmwt.scales[0] * sizeof(int*));
    iprfs = malloc(pmwt.scales[0] * sizeof(float*));
    bstds = malloc(pmwt.scales[0] * sizeof(float*));
    psel  = calloc(pmwt.scales[0],  sizeof(int));

    if (mcount)
    {
        for (int k = 0; k < pmwt.scales[0]; k++)
        {
            sprintf(buf, "%s.slopes.%d.log", opref, pmwt.scales[k + 1]);
            slog = fopen(buf, "w+");

            px = malloc(pmwt.bw * pmwt.N * sizeof(int));
            py = malloc(pmwt.bw * pmwt.N * sizeof(int));
            dbImg = getDriftBands(pmwt, pval, k, px, py, &Npts);

            sprintf(buf, "%s.masks.%d.log",  opref, pmwt.scales[k + 1]);
            mlog = fopen(buf, "w+");

            fprintf(mlog, "# %d\t%d\t%d\n", pmwt.scales[k + 1], pmwt.N, pmwt.Nb);

            for (int i = 0; i < pmwt.N; i++)
            {
                for (int j = 0; j < (pmwt.Nb - pmwt.bw) / 2; j++)
                    fprintf(mlog, "0, ");
                for (int j = 0; j < pmwt.bw; j++)
                    fprintf(mlog, "%d, ", dbImg[i * pmwt.bw + j]);
                for (int j = 0; j < (pmwt.Nb - pmwt.bw) / 2; j++)
                    fprintf(mlog, "0, ");
                fprintf(mlog, "\n");
            }

            fclose(mlog);

            ht1 = HoughTransform(dbImg, pmwt.N, pmwt.bw, nAngles);
            angleNo = (int) roundf((float) nAngles / 2.0 + atan(((-1) * (float) pmwt.Nb 
                            * modes[k + 1] / 1000) / (float) pmwt.P) 
                            * ((float) nAngles) / pi);
            ind = calloc(pmwt.N, sizeof(int));
            Nind = locMinInd(&ht1[angleNo], pmwt.N, nAngles, ind, 1.0);
            theta = pi * ((float) angleNo / ((float) nAngles) - 0.5);
            slope = -1 / tan(theta);
            intercepts = calloc(Nind, sizeof(float));
            for (int i = 0; i < Nind; i++)
                intercepts[i] = (float) ind[i] / sin(theta);
            partitions = malloc(Nind * sizeof(double*));
            for (int i = 0; i < Nind; i++)
                partitions[i] = calloc(1 + 1024 * 2, sizeof(double));
            for (int i = 0; i < Npts; i++)
            {
                for (int j = 0; j < Nind; j++)
                {
                    tmp1[0] = slope; tmp1[1] = intercepts[j];
                    tmp2[0] = px[i]; tmp2[1] = py[i];
                    dist = distlp(tmp1, tmp2);
                    if (dist < thres_factor)
                    {
                        partitions[j][(int) ++partitions[j][0]] = (double) px[i];
                        partitions[j][(int) ++partitions[j][0]] = (double) py[i];
                    }
                }
            }
            lines = calloc(2 * Nind, sizeof(double));
            nlines = 0;

            fprintf(slog, "# P3 = %0.2fP\n", (1 /
                      stof(w0, pmwt.N, pmwt.T, scales[k + 1])) / pmwt.P);
            fprintf(slog, "# Slope\t\t\tSlope\t\t\tIntercept\t\tSumSq\t\tNpts\n"
                          "# (bins/period)\t(ms/period)\t\t(periods)\n");
            
            for (int i = 0; i < Nind; i++)
            {
                if (partitions[i][0] / 2 > 4)
                {
                    gsl_fit_linear(&partitions[i][1], 2, &partitions[i][2], 2, 
                                    partitions[i][0] / 2, &lines[2 * nlines + 0], 
                                    &lines[2 * nlines + 1], &cov00, &cov01, &cov11, &chisq);
                    if (fabs(lines[2 * nlines + 1]) < 0.01)
                        continue;
                    fprintf(slog, "%f\t\t%f\t\t%f\t\t%f\t%d\n", lines[2 * nlines + 1],
                                   1000.0 * pmwt.P/(pmwt.Nb *   lines[2 * nlines + 1]), 
                                   lines[2 * nlines + 0], chisq, (int) partitions[i][0] / 2);
                    nlines++;
                }
            }
            printf("Bands: %d\tAverage slope = %g.\n", nlines, 
                          gsl_stats_mean(&lines[1], 2, nlines));
        
            masks[k] = getModeMap(partitions, Nind, pmwt.N);

            free(px);
            free(py);
            free(ht1);
            free(dbImg);
            free(intercepts);
            free(ind);
            free(lines);
            for (int i = 0; i < Nind; i++)
                free(partitions[i]);
            free(partitions);
            fclose(slog);
        }
    } else {
        float *s_max = calloc(scales[0], sizeof(float));
        float *s_min = calloc(scales[0], sizeof(float));

        for (int i = 0; i < scales[0]; i++)
            masks[i] = calloc(pmwt.N, sizeof(int));

        for (int i = 0; i < scales[0]; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (pmwt.powsums[i][j] > s_max[i])
                    s_max[i] = pmwt.powsums[i][j];
                else if (pmwt.powsums[i][j] < s_min[i])
                    s_min[i] = pmwt.powsums[i][j];
            }
        }

        for (int i = 0; i < scales[0]; i++)
            for (int j = 0; j < N; j++)
                if (pmwt.powsums[i][j] * thres_factor > s_max[i])
                    masks[i][j] = 1;
    }

    cmask = cleanMasks(masks, scales[0], pmwt.N, psel);

    nPi = calloc(scales[0], sizeof(int));

    sprintf(buf, "%s.periods.log", opref); 
    FILE *plog = fopen(buf, "w+");

    for (int k = 0; k < scales[0]; k++)
    {
        iprfs[k] = calloc(pmwt.Nb, sizeof(float));
        bstds[k] = calloc(pmwt.Nb, sizeof(float));
        vals     = calloc(pmwt.Nb * psel[k], sizeof(float));
        vc = 0;  
        sprintf(buf, "%s-%02d.dat", opref, scales[k + 1]);
        f = fopen(buf,   "w+");
        
        fprintf(plog, "# P3 = %0.2fP\n", (1 / 
                      stof(w0, pmwt.N, pmwt.T, scales[k + 1])) / pmwt.P);
        
        vc = 0;
        for (int i = 0; i < pmwt.N; i++)
        {
            if (cmask[k][i])
            {
                fprintf(plog, "%d\n", i);
                fprintf(f, "# 0 0 %f 0 0 0 %d ? 1\n", pmwt.P, pmwt.Nb);
                for (int j = 0; j < pmwt.Nb; j++)
                {
                    fprintf(f, "%d %f\n", j, dat.data[i * dat.Nbins + j]);
                    iprfs[k][j] += dat.data[i * dat.Nbins + j];
                    vals[vc]     = dat.data[i * dat.Nbins + j];
                    vc++;
                }
                nPi[k]++;
            }   
        }

        for (int i = 0; i < pmwt.Nb; i++)
            bstds[k][i] = gsl_stats_float_sd(&vals[i], pmwt.Nb, vc / pmwt.Nb) 
                                                      / sqrt(vc / pmwt.Nb - 1);
        
        fclose(f);

        printf("Scale: %d - Number of periods integrated = %d.\n", pmwt.scales[k + 1], nPi[k]);
        for (int j = 0; j < pmwt.Nb; j++)
            iprfs[k][j] /= nPi[k];

        free(vals);
    }

    fclose(plog);
   
    if (limits[0] == 0 || limits[1] < 0 || limits[2] > pmwt.Nb)
    {
        if (limits[0] != 0)
            fprintf(stderr, "Invalid period range: %d-%d, using default.\n", 
                limits[1], limits[2]);
        limits[1] = 0, limits[2] = pmwt.Nb;
    }

    plotIProf(iprfs, pmwt.scales, nPi, pmwt.scales[0], pmwt.Nb, 
              ititle ? ititle : opref, "?", limits);

    sprintf(buf, "%s.prof.log", opref);
    FILE *ilog = fopen(buf, "w+");

    fprintf(ilog, "# Bin\t");
    for (int i = 1; i <= scales[0]; i++)
        fprintf(ilog, "%d\t\t\tSE\t\t\t", scales[i]);
    fprintf(ilog, "\n");

    for (int i = 0; i < pmwt.Nb; i++)
    {
        fprintf(ilog, "%d\t\t", i);
        for (int j = 0; j < scales[0]; j++)
            fprintf(ilog, "%f\t%f\t", iprfs[j][i], bstds[j][i]);
        fprintf(ilog, "\n");
    }

    fclose(ilog);


    for (int i = 0; i < scales[0]; i++)
    {
        free(masks[i]);
        free(cmask[i]);
        free(iprfs[i]);
        free(bstds[i]);
    }


    destroypsrMWT(&pmwt);
    free(opref);
    free(masks);
    free(cmask);
    free(iprfs);
    free(bstds);
    free(psel);

    free(nPi);
    if (pulsardat)
        free(dat.data);

    return 0;
}

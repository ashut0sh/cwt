#!/usr/bin/python2

import sys
import struct
import numpy as np
from pylab import *
from subprocess import call

out = open(str(sys.argv[1]) if len(sys.argv) == 2 else "test.txt", 'w')

M = 10     # Number of repetitions for each amplitude of noise
N = 65536  # Number of points
J = 192    # Number of scales
T = 20     # Time in seconds

out.write("""RMS Error\tphi1\t\tMorlet\t\tDiff %\t\tMexican\t\tDiff %\t\tphi2\t\tMorlet\t\tDiff %\t\tMexican\t\tDiff %\n""")

np.set_printoptions(precision=5)
# Amplitudes of Gaussian noise to be tested. 
# vals = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 2.0, 5.0, 10.0];
vals = [0.0, 0.5, 1.0];

for i in vals:
    print "Doing error level: ", i;
    d1_mor, d2_mor, d1_mex, d2_mex = [], [], [], [];  # Lists which will hold the absolute errors.
    noise = i * np.random.normal(0, 1, N)  # Generate the noise of the required amplitude.
    for j in xrange(M):
        # Randomly choose the phases
        phi1, phi2 = np.pi * (-1 + 2 * np.random.random(2))
        
        # Generate the signal and add the noise
        x = linspace(0, T, N);
        y = empty(N);
        y[:N/2] = np.cos(2 * np.pi * 0.4 * x[:N/2] + phi1);
        y[N/2:] = np.cos(2 * np.pi * 0.8 * x[N/2:] + phi2);
        z = y + noise;
        
        # Save the signal to a file, take wavelet transform, and read its output.
        np.savetxt("file.dat", z, delimiter="\n");
        call(["./cwt", "-i", "file.dat", "-N", str(N), "-J", str(J), 
                "-T", str(T), "-o", "file1.cwt", "-P", str(1000), "-W", str(0)])
        call(["./cwt", "-i", "file.dat", "-N", str(N), "-J", str(J),
                "-T", str(T), "-o", "file2.cwt", "-P", str(1000), "-W", str(1)])
        with open("file1.cwt", mode="rb") as f1:
            data1 = f1.read()
        with open("file2.cwt", mode="rb") as f2:
            data2 = f2.read();

        # Extract the phases at the center of the signal and append them to the list.
        x = 4 * (5 + N * (J + 130) + 1 * N / 4)
        phi1_mor = struct.unpack('f', data1[x : x + 4])[0]
        x = 4 * (5 + N * (J + 125) + 1 * N / 4)
        phi1_mex = struct.unpack('f', data2[x : x + 4])[0]
        x = 4 * (5 + N * (J + 120) + 3 * N / 4)
        phi2_mor = struct.unpack('f', data1[x : x + 4])[0]
        x = 4 * (5 + N * (J + 115) + 3 * N / 4)
        phi2_mex = struct.unpack('f', data2[x : x + 4])[0]
        d1mor, d2mor = (abs(phi1) - abs(phi1_mor)) / phi1, (abs(phi2) - abs(phi2_mor)) / phi2;
        d1mex, d2mex = (abs(phi1) - abs(phi1_mex)) / phi1, (abs(phi2) - abs(phi2_mex)) / phi2;
        d1_mor.append(d1mor); d2_mor.append(d2mor);
        d1_mex.append(d1mex); d2_mex.append(d2mex);
        out.write("%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n" % 
                  (i, phi1, phi1_mor, d1mor, phi1_mex, d1mex, 
                      phi2, phi2_mor, d2mor, phi2_mex, d2mex));
    out.write("%f\t\t\t\t\t\t%f\t\t\t%f\t\t\t\t\t\t%f\t\t\t%f\n\n" % 
                (i, np.mean(np.abs(d1_mor)), np.mean(np.abs(d1_mex)), 
                    np.mean(np.abs(d2_mor)), np.mean(np.abs(d2_mex))));
out.close()

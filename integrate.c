#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <cpgplot.h>

#include "utils.h"

// Generate folded profile of provided data of N samples, with R as the ratio
// of the sampling rate and the period, into M bins.
float* fold(float *data, const float R, const int M, const int N)
{
    float *folded  = calloc(sizeof(float), N), tmp; 

    for (int i = 0; i < M; i++)
        folded[(int) roundf(modff(i * R, &tmp) * N)] += data[i];

    return folded;
}

// Generate integrated plots at a provided scale, using the samples p_y.
int plotIP(const float *p_y, const int N, const char *label, const int scale)
{
    char format_spec[64], title[64];
    sprintf(format_spec, "%s_%03d.png/PNG", label, scale);
    sprintf(title, "Integrated Profile: %s, Scale: %d.", label, scale);

    float min = p_y[0], max = p_y[0];
    float *p_x = calloc(N, sizeof(float));

    for (int i = 0; i < N; i++)
    {
        p_x[i] = (float) i;
        if (p_y[i] > max)
            max = p_y[i];
        else if (p_y[i] < min)
            min = p_y[i];
    }

    if (cpgbeg(0, format_spec, 1, 1) != 1)
        return 1;

    cpgscr(0, 1, 1, 1);
    cpgscr(1, 0, 0, 0);
    cpgscf(2);
    cpgsch(1.5);
    cpgenv(0.0, N, 0.0, max, 0, 1);
    cpglab("Bin", "Count", title);
    cpgline(N, p_x, p_y);
    cpgend();

    free(p_x);
    return 0;
}

// Generates drift plot: Showing only mutually exclusive data from specified
// scales.
// TODO: More testing.
int *driftPlot(const float *p_x, const float *p_y, const int N, const int Np,
              const int *scales, const int M, const int w, const int J,
              const int J0, const char *label)
{
    int *map = calloc(sizeof(int), (J - J0) * Np), sum = 0, sc = 0;
    float *px, *py;
    int flag1;

    char format_spec[64], title[64];
    sprintf(format_spec, "%s_DP.png/PNG", label);
    sprintf(title, "Drift Plot: %s", label);

    for (int i = 0; i < N; i++)
        map[((int) p_y[i] - J0) * Np + (int) p_x[i]] = 1;

    for (int i = 0; i < M; i++)
        for (int j = 0; j < Np; j++)
            for (int k = scales[i] - J0 - w; k <= scales[i] -J0 + w && scales[i] + w < (J - J0); k++)
                if (map[k * Np + j] == 1)
                    map[(scales[i] - J0)* Np + j] = 1;

    for (int i = 0; i < J - J0; i++)
    {
        flag1 = 0;
        for (int j = 0; j < M; j++)
            if (scales[j] == i + J0)
                flag1 = 1;

        if (!flag1)
            for (int j = 0; j < Np; j++)
                map[i * Np + j] = 0;
    }
/*
    for (int i = 0; i < M; i++)
        for (int j = i + 1; j < M; j++)
        {
            printf("Doing: (%d, %d)\n", i, j);
            for (int k = 0; k < Np; k++)
                if (map[(scales[i] - J0) * Np + k] && map[(scales[j] - J0) * Np + k])
                {
                    map[(scales[i] - J0) * Np + k] = 0;
                    map[(scales[j] - J0) * Np + k] = 0;
                }
        }
*/
    for (int i = 0; i < (J - J0) * Np; i++)
        sum += map[i];

    px = calloc(sizeof(float), sum);
    py = calloc(sizeof(float), sum);

    for (int i = 0; sc < sum && i < (J - J0); i++)
        for (int j = 0; j < Np; j++)
            if (map[i * Np + j])
            {
                px[sc] = (float) j;
                py[sc] = (float) (i + J0);
                sc++;
            }


    if (sc != sum)
        fprintf(stderr, "Count error: %d, %d.\n", sc, sum);
    
    if (cpgbeg(0, format_spec, 1, 1) != 1)
        return map;

    cpgscr(0, 1, 1, 1);
    cpgscr(1, 0, 0, 0);
    cpgscf(2);
    cpgsch(1.5);
    cpgenv(0.0, Np, J, J0, 0, 1);
    cpglab("Periods", "Scale", label);
    cpgpt(sum, px, py, -1);
    cpgend();

    return map;
}

int main(int argc, char *argv[])
{
    int opt, k, Si, J, J0, s = 0; // Cmin = 50, count = 0, *on;
    int p3[16], p3c = 0; // p3present = 0;
    float *p_x, *p_y, *periods; // *folded, scale,  Wcoef = 1;
    char *pfile = NULL, *dfile = NULL, *opref = NULL, *tmp = NULL;

    psrDAT dat;
    FILE *p;

    if (argc < 6 || argc > 15)
    {
        fprintf(stderr, "Check arguments: %d\n", argc);
        return 1;
    }

    while ((opt = getopt(argc, argv, "p:d:n:O:w:P:S:")) != -1)
    {
        switch (opt)
        {
            case 'p':    // PTS input file.
                pfile = strdup(optarg);
                break;
            case 'd':    // Single pulse data file.
                dfile = strdup(optarg);
                break;
            case 'n':    // Minimum length of feature, in periods, to integrate.
//              Cmin = atoi(optarg);
                break;
            case 'O':    // Output file prefix. 
                opref = strdup(optarg);
                break;
            case 'w':    // Wavelet coefficient - currently not required.
//              Wcoef = atof(optarg);
                break;
            case 'P':    // Comma separated list of scales of interest.
                p3[p3c++] = atoi(strtok(optarg, ","));
                while ((tmp = strtok(NULL, ",")) != NULL)
                    p3[p3c++] = atoi(tmp);
                break;
            case 'S':
                s = atoi(optarg);
                break;
        }
    }

    if ((p = fopen(pfile, "rb")) == NULL)
    {
        perror("Failed to open PTS file:");
        exit(1);
    }

    fread(&J  , sizeof(int),   1, p);
    fread(&J0 , sizeof(int),   1, p);
    fread(&k  , sizeof(int),   1, p);
    p_x = calloc(k, sizeof(float));
    p_y = calloc(k, sizeof(float));
    fread(p_x, sizeof(float), k, p);
    fread(p_y, sizeof(float), k, p);

    fclose(p);

    readDAT(dfile, &dat);

    Si = round(10 * log2f(((float) dat.Nperiods)));

    printf("Ignored base-scale = %d.\n", Si);

/*    for (int i = 0; i < k; i++)
    {
        scale = p_y[i];

        if (scale == Si      || scale == Si - 9  || scale == Si - 10 ||
            scale == Si - 11 || scale == Si - 15 || scale == Si - 16 ||
            scale == Si - 17 || scale == Si - 19 || scale == Si - 20 ||
            scale == Si - 21)
            continue;
        
        p3present = 0;
        if (pspec)
        {
            for (int i = 0; i < p3c; i++)
                if (p3[i] == scale)
                    p3present = 1;

            if (!p3present)
                continue;
        }

        count = 0;
        on = calloc(dat.Nperiods, sizeof(int));
        for (int j = 0; j < dat.Nperiods && i < k && scale == p_y[i]; j++)
        {
            if (p_x[i] == j)
            {
                ++on[j];
                ++count;
                ++i;
            }
        }

        if (count < Cmin)
        {   
            free(on);
            continue;
        }

        periods = calloc(dat.Nbins, sizeof(float));
        
        for (int l = 0; l < dat.Nperiods; l++)
        {
            if (!on[l])
                continue;
            for (int m = 0, count = 0; m < dat.Nbins; m++, count++)
                periods[count] += dat.data[l * dat.Nbins + m];
        }

        plotIP(periods, dat.Nbins, opref, scale);
        
        free(periods);
        free(on);
    }
*/
    int *py = driftPlot(p_x, p_y, k, dat.Nperiods, p3, p3c, s, J, J0, opref);

    int cfa;
    FILE *f3;
    char nm[32];
    for (int i = 0; i < p3c; i++)
    {
        periods = calloc(dat.Nbins, sizeof(float));
        cfa = 0;
        for (int j = 0; j < dat.Nperiods; j++)
            if (py[(p3[i] - J0) * dat.Nperiods + j])
            {
                cfa++;

                for (int k = 0; k < dat.Nbins; k++)
                    periods[k] += dat.data[j * dat.Nbins + k];
            }
        printf("%d points at scale %d.\n", cfa, p3[i]);

        sprintf(nm, "/tmp/%03d.txt", p3[i]);
        f3 = fopen(nm, "w");

        for (int i = 0; i < dat.Nbins; i++)
            fprintf(f3, "%f\n", periods[i]);

        fclose(f3);

        plotIP(periods, dat.Nbins, opref, p3[i]);
        free(periods);
    }

    free(p_x);
    free(p_y);
    free(pfile);
    free(dfile);
    free(opref);
    free(dat.data);
    free(py);

    return 0;
}

#include <math.h>
#include <time.h>
#include <fftw3.h>
#include <unistd.h>
#include <gsl/gsl_statistics_double.h>

#ifndef __CWT_H
#define __CWT_H

#define SQR(X) ((X) * (X))
#define CSVFMT(VAR, MAX) (VAR + 1 != MAX) ? "," : "\n"
#define FFTABS(A) sqrt(SQR(A[0]) + SQR(A[1]))
#define FFTTAN(A) atan2(A[IMAG], A[REAL])

#define FALSE 0
#define TRUE  1
#define REAL  0
#define IMAG  1

// struct of arguments to pthread_create to generate Morlet wavelet
typedef struct mrunner_args 
{
    fftw_complex *f_morlet;
    float w0;
    int N, M, J, J0;
    int j1, j2;
} mrunner_args;

// struct of arguments to pthread_create to perform convolution
typedef struct mulrunner_args
{
    fftw_complex *in, *out, *B;
    int N, J, J0;
    int j1, j2;
} mulrunner_args;

void* runner_mul(void *arg);
void* runner_m  (void *arg);

void phase_unwrap(float *w, int N);

fftw_complex *morlet_fft (const int N, const int J, const int J0, const float w0);
fftw_complex *mexican_fft(const int N, const int J, const int J0, const float w0);

float *cwt(double *in, fftw_complex *B, const int N, 
           const int J, const int J0, const int unwrap);

#endif // __CWT_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <strings.h>
#include <cpgplot.h>
#include <gsl/gsl_cdf.h>

#include "utils.h"

#define SQR(X) ((X) * (X))
#define CSVFMT(VAR, MAX) (VAR + 1 != MAX) ? ", " : "\n"

typedef enum {AMPLITUDE, PHASE, POWER, BIN} plotType;

// Writes extracted CWT data to specified CSV file.
void write_csv(float *data, const int N, const int J, const int J0, 
               const float T, const char *path)
{
    FILE *f = fopen(path, "w+");

    fprintf(f, "N = %d\tJ = %d\tJ0 = %d\tT= %f\n", N, J, J0, T);

    for (int i = 0; i < (J - J0); i++)
        for (int j = 0; j < N; j++)
            fprintf(f, "%.4f%s", data[i * N + j], CSVFMT(j, N));
    
    for (int i = (J - J0); i < 2 * (J - J0); i++)
        for (int j = 0; j < N; j++)
            fprintf(f, "%.4f%s", data[i * N + j], CSVFMT(j, N));

    fclose(f);
}

// Plot heatmap of provided data.
void plot(float *spec, float *data, const int N, const int J, const int J0, const float T,
          const float w0, const float P, const char *format_spec, const char *label, 
          const int *lims, plotType p)
{
    float tr[] = { 0.0, 1.0,  0.0,  0.0,  0.0, 1.0};
    float rl[] = {-0.5, 0.0, 0.17, 0.33, 0.50, 0.67, 0.83, 1.0, 1.7};
    float rr[] = { 0.0, 0.0,  0.0,  0.0,  0.6,  1.0,  1.0, 1.0, 1.0};
    float rg[] = { 0.0, 0.0,  0.0,  1.0,  1.0,  1.0,  0.6, 0.0, 1.0};
    float rb[] = { 0.0, 0.3,  0.8,  1.0,  0.3,  0.0,  0.0, 0.0, 1.0};

    float max = spec[0], min = spec[0];
    for (int i = lims[2]; i < lims[3]; i++)
    {
        for (int j = lims[0]; j < lims[1]; j++)
        {
            if (spec[i * N + j] > max)
                max = spec[i * N + j];
            else  if (spec[i * N + j] < min)
                min = spec[i * N + j];
        }
    }

    float *fin = calloc(N * J, sizeof(float));
    float *sum = calloc(1 * J, sizeof(float));
    float *axx = calloc(1 * J, sizeof(float));

    memcpy(&fin[J0 * N], spec, sizeof(float) * (J - J0) * N);

    for (int i = lims[2]; i < lims[3]; i++)
    {
        for (int j = lims[0]; j < lims[1]; j++)
        {
            if (p == POWER)
                sum[i] += data[i * N + j];
            else
                sum[i] += fin[i * N + j];

            if (P == -1)
                axx[i] = log10(stof(w0, N, T, (float) i));
            else
                axx[i] = log10(( 1 / stof(w0, N, T, (float) i)) / P);
        }
    }

    float max_s = sum[0], min_s = sum[0];
    
    for (int i = 0; i < J; i++)
        if (sum[i] > max_s)
            max_s = sum[i];
        else if (sum[i] < min_s)
            min_s = sum[i];

    if (cpgbeg(0, format_spec, 1, 1) != 1)
        exit(2);

    cpgscr(0, 1, 1, 1);
    cpgscr(1, 0, 0, 0);
    cpgscf(2);
    cpgsch(1.5);
    cpgsvp(p != PHASE ? 0.10 : 0.15, p != PHASE ? 0.75 : 0.90, 0.25, 0.80);
    cpgswin(lims[0], lims[1], lims[3], lims[2]);
    cpgbox("ABCINT", 0.0, 0.0, "ABCINT", 0.0, 0.0);
    cpgctab(rl, rr, rg, rb, 9, 1.0, 0.5);
    cpgsch(2.5);
    cpgmtxt("T", 1.0, 0.62, 0.5, label);
    cpgsch(2.0);
    cpgmtxt("L", 2.0, 0.5, 0.5, "Scale");
    cpgmtxt("B", 2.0, 0.5, 0.5, P == -1 ? "Translation" : "Period Number");
    cpgimag(fin, N, J, lims[0], lims[1], lims[2], lims[3], min / 100, max, tr);
    cpgwedg("BI", 2.5, 3, min / 100, max, "Power");
    cpgsch(1.5);
    if (p != PHASE)
    {
        cpgsvp(0.75, 0.90, 0.25, 0.80);
        cpgswin(1.1 * max_s, min_s, axx[lims[3] - 1], axx[lims[2]]);
        cpgbox("ABCN", 0.0, 0.0, "ABCLMS", 0.0, 0.0);
        cpgsch(2.0);
        cpgmtxt("B", 2.0, 0.5, 0.5, "Sum");
        cpgsch(1.5);
        cpgline(lims[3] - lims[2], &sum[lims[2]], &axx[lims[2]]);
    } else {
        cpgsvp(0.10, 0.90, 0.17, 0.90);
        cpgswin(lims[0], lims[1], axx[lims[3] - 1], axx[lims[2]]);
        cpgbox("", 0.0, 0.0, "ABCLMS", 0.0, 0.0);
    }
    cpgsch(2.0);
    cpgmtxt("R", 2.5, 0.5, 0.5, P == -1 ? "Frequency (Hz)" : "Periodicity (P0)");
    cpgend();

    free(fin);
    free(sum);
    free(axx);
}

// xy plot of specified scale.
void plot_scale(float *scale, const int N, const float T, 
                const char *format_spec, const char *label, const int i)
{
    float *xaxis = linspace_f(0, T, N); 

    float max = scale[0], min = scale[0];
    for (int i = 0; i < N; i++)
    {
        if (scale[i] > max)
            max = scale[i];
        else if (scale[i] < min)
            min = scale[i];
    }

    if (min != max)
    {
        if (cpgbeg(0, format_spec, 1, 1) != 1)
            exit(2);

        cpgscr(0, 1, 1, 1);
        cpgscr(1, 0, 0, 0);
        cpgscf(2);
        cpgsch(1.5);
        cpgenv(0.0, T, min, max, 0, 2);
        cpglab("Time (s)", "Phase (rad)", label);
        cpgpt(N, xaxis, scale, -1);
        cpgend();
    } else {
        fprintf(stderr, "Empty plot: Not plotting for scale: %d.\n", i);
    }

    free(xaxis);
}

// Ridge plot: plot local maxima of amplitude data along the scale axis.
// Also generates a points.pts file, to be read by the utility integrate
// to generate integrated plots along provided scales for extracting modes.

void plot_ridges(float *data, const int N, const int J, const int J0, 
                 const int T, const float w0, const float P,
                 const char *format_spec, const char *label, const int *lims)
{
    float *p_x = calloc(sizeof(float), N * (J - J0));
    float *p_y = calloc(sizeof(float), N * (J - J0));
    int pcount = 0;

    for (int i = 1; i < J - J0; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (data[(i - 1) * N + j] < data[i * N + j]  &&
                data[(i + 1) * N + j] < data[i * N + j]) 
            {
                p_x[pcount] = (float) j;
                p_y[pcount] = (float) i + J0;
                pcount++;
            }
        }
    }

    printf("Points used: %d.\n", pcount);

    float *sum = calloc(pcount, sizeof(float));
    float *axx = calloc(pcount, sizeof(float));
    float max_s = 0, min_s = 0;

    for (int i = 0; i < pcount; i++)
        if (p_x[i] > lims[0] && p_x[i] < lims[1] &&
            p_y[i] > lims[2] && p_y[i] < lims[3])
            sum[(int) p_y[i]] += 1;

    for (int i = 0; i < J; i++)
    {
        if (P == -1)
            axx[i] = log10(stof(w0, N, T, (float) i));
        else
            axx[i] = log10(( 1 / stof(w0, N, T, (float) i)) / P);

        if (sum[i] > max_s)
            max_s = sum[i];
        else if (sum[i] < min_s)
            min_s = sum[i];
    }
    
    if (cpgbeg(0, format_spec, 1, 1) != 1)
        exit(2);

    cpgscr(0, 1, 1, 1);
    cpgscr(1, 0, 0, 0);
    cpgscf(2);
    cpgsch(1.0);
    cpgsvp(0.05, 0.75, 0.17, 0.90);
    cpgswin(lims[0], lims[1], lims[3], lims[2]);
    cpgbox("ABCN", 0.0, 0.0, "ABCN", 0.0, 0.0);
    cpglab("Translation", "Scale", label);
    cpgpt(pcount, p_x, p_y, -1);
    cpgsvp(0.75, 0.90, 0.17, 0.90);
    cpgswin(1.05 * max_s, min_s, axx[lims[3] - 1], axx[lims[2]]);
    cpgbox("ABCM", 0.0, 0.0, "ABCLMS", 0.0, 0.0);
    cpglab("Sum", "", "");
    cpgmtxt("R", 2.5, 0.5, 0.5, P == -1 ? "Frequency (Hz)" : "Periodicity (P0)");
    cpgline(J - J0, &sum[J0], &axx[J0]);
    cpgend();

    free(p_x);
    free(p_y);
    free(sum);
    free(axx);
}

// Phase scale plots for multiple phase, wrapper around plot_scales.
void make_scale_plots(float *data, const int N, const int J, const int J0, 
                      const float T, int *scales, const int nums, 
                      const char *path, const char *format)
{
    float *scale = calloc(sizeof(float), N);
    const int str_sz = 32;
    
    char name[str_sz], label[str_sz];
    memset(name,  0, sizeof(char) * str_sz);
    memset(label, 0, sizeof(char) * str_sz);

    for (int i = 0; i < nums; i++)
    {
        if (strcasecmp(format, "xwindow") == 0 || 
            strcasecmp(format, "xserve") == 0)
            sprintf(name, "/%s", format);
        else
            sprintf(name, "%s_phase_%03d.%s/%s", path, scales[i], format, format);

        sprintf(label, "Phase at scale = %d", scales[i]);

        memcpy(scale, &data[N * ((J - J0) + scales[i])], N * sizeof(float));

        plot_scale(scale, N, T, name, label, scales[i]);
    }

    free(scale);
}

// Makes all the plots, wrapper around plot() and plot_ridges().
void make_plots(float *data, float *sdat, const int N, const int J, const int J0,
                const float T, const float w0, const float P,  const float siglevel,
                const float signif, const char *path, const char *format, 
                const int *lims, const int poweronly, const int sigpoweronly)
{
    float *spec = calloc(sizeof(float), N * (J - J0));
    const int str_sz = 32;

    char amplstring[str_sz], phasestring[str_sz], ridgestring[str_sz], powstring[str_sz];
    char amplabel[str_sz], phaselabel[str_sz], ridgelabel[str_sz], powlabel[str_sz];
    
    memset(amplstring,  0, str_sz * sizeof(char));
    memset(phasestring, 0, str_sz * sizeof(char));
    memset(ridgestring, 0, str_sz * sizeof(char));
    memset(powstring,   0, str_sz * sizeof(char));

    sprintf(amplabel,   "Amplitude");
    sprintf(powlabel,   "Power (Significance: %.2f)", siglevel);
    sprintf(ridgelabel, "Ridge Plot (Significance: %.2f)", siglevel);
    sprintf(phaselabel, "Phase (Significance: %.2f)", siglevel);

    if (strcasecmp(format, "xwindow") == 0 || 
        strcasecmp(format, "xserve") == 0)
    {
        sprintf(amplstring,  "/%s", format);
        sprintf(powstring,   "/%s", format);
        sprintf(phasestring, "/%s", format);
        sprintf(ridgestring, "/%s", format);
    } else {
        sprintf(amplstring,  "%s_amplitude.%s/%s", path, format, format);
        sprintf(powstring,   "%s_power.%s/%s", path, format, format);
        sprintf(phasestring, "%s_phase.%s/%s", path, format, format);
        sprintf(ridgestring, "%s_ridge.%s/%s", path, format, format);
    }

    if (!poweronly)
    {
        // Amplitude first
        for (int i = 0; i < N * (J - J0); i++)
            spec[i] = data[i];
    
        plot(data, data, N, J, J0, T, w0, P, amplstring, amplabel, lims, AMPLITUDE);
        plot_ridges(sdat, N, J, J0, T, w0, P, ridgestring, ridgelabel, lims);

        // Phase now
        for (int i = 0; i < N * (J - J0); i++)
            spec[i] = sdat[N * (J - J0) + i];

        plot(spec, data, N, J, J0, T, w0, P, phasestring, phaselabel, lims, PHASE);
    }

    // Power finally, divided by scale to normalize.
    // See http://ocgweb.marine.usf.edu/~liu/wavelet.html .
    for (int i = 0; i < J - J0; i++)
        for (int j = 0; j < N; j++)
            spec[i * N + j] = SQR(sdat[i * N + j]) / pow(2, 0.1 * (J0 + i));

    plot(spec, data, N, J, J0, T, w0, P, powstring,  powlabel, lims, 
            sigpoweronly ? POWER : AMPLITUDE); 

    free(spec);
}

void validateLims(const int lc, int *lims, const int x1, 
                    const int y0, const int y1)
{
    if (lc)
    {
        if (lims[0] > x1 || lims[0] < 0       || 
            lims[1] > x1 || lims[1] < lims[0] || 
            lims[2] > y1 || lims[2] < y0      || 
            lims[3] > y1 || lims[3] < lims[2])
        {
            fprintf(stderr, "Invalid scale limits.");
            exit(1);
        } else {
            if (lims[0] == 0)
                lims[0] = 1;
            if (lims[1] == 0)
                lims[1] = x1 - 1;
            if (lims[2] == 0)
                lims[2] = y0 + 1;
            if (lims[3] == 0)
                lims[3] = y1 - 1;
        }
    } else {
        lims[0] =      1; lims[1] = x1 - 1;
        lims[2] = y0 + 1; lims[3] = y1 - 1;
    }
}

void print_usage_exit()
{
    fprintf(stderr, "USAGE: wavreader   -i <input CWT file>\n"
                    "                   -o <file output name without extension\n"
                    "                   -f <format/driver for PGPLOT\n"
                    "                   -l <comma separated values of initial and final translation and scale\n"
                    "                       (eg. 1000,2000,10,20 or ,,10,20 to use default)>\n"
                    "                   -r <initial value of scale for phase-translation plot>\n"
                    "                   -R <final   value of scale for phase-translation plot>\n"
                    "                   -t <comma separated list of scales for phase-translation plot (eg. 25,30,35)\n"
                    "                   -F <no options; show power below significance in cumulative FFT of power plot>\n"
                    "                   -N <no options; no plots>\n"
                    "                   -O <name of CSV file (with extension if required) to write output to.\n"
                    "                      Output will hold a header with basic information and two blocks,  \n"
                    "                      amplitudes and phases, at all translations and scales present in the CSV file.\n"
                    "                   -s <significance level between 0 and 1; default=0.95>\n"
                    "                   -p <no options; plot power only>\n"
                    "                   -m <comma separated list of cwt files to combine into a power plot.\n"
                    "                       (e.g. -m file1.cwt,file2.cwt,file3.cwt)>");
    exit(1);
}


int main(int argc, char **argv)
{
    int n1 = 0, n2 = 0, noplot = 0, writecsv = 0, list = 0, poweronly = 0;
    int lims[4] = {-1}, lc = 0, sigpoweronly = 0;
    float signif, siglevel = 0.95, *sdat;
    char *ifile = NULL, *ofile = NULL, *driver = NULL, *csvfile = NULL, *cwtfile = NULL;
    char *lscales = NULL, *tmp = NULL, *val = NULL, *itmp = NULL, *cwtfiles = NULL;
    char *mwtfile = NULL, *ititle = NULL;

    psrCWT pcwt = {NULL, 0.0, 0.0, 0, 0, 0, 0, 0.0, 0.0};
    psrMWT pmwt = {NULL, NULL, 0.0, 0, 0, NULL, NULL, 0, 0, 0, 0.0, NULL};

    if (argc < 2)
        print_usage_exit();

    int opt;
    while ((opt = getopt(argc, argv, "i:Ff:o:r:R:l:L:m:M:t:NO:ps:")) != -1)
    {
        switch (opt)
        {
            case 'i': // Input file
                ifile = strdup(optarg);
                break;
            case 'o': // Image output name
                ofile =  strdup(optarg);
                break;
            case 'f': // Image output format
                driver =  strdup(optarg);
                if (strcasecmp(driver, "xwindow") == 0 ||
                    strcasecmp(driver, "xserve")  == 0)
                {
                    ofile = malloc(1);
                    memset(ofile, 0, 1);
                }
                break;
            case 'l': // Zoom into region of plot
                itmp = strdup(optarg);
                tmp = itmp;
                lims[lc] = atoi(strsep(&tmp, ","));
                while ((val = strsep(&tmp, ",")) != NULL && lc <= 3)
                    lims[++lc] = atoi(val);
                if (lc < 3)
                {
                    fprintf(stderr, "Please specify 4 limits: t_init, t_final, j_init, j_final.\n");
                    exit(1);
                }
                free(itmp);
                break;

            case 'r': // List of scales
                n1 = atoi(optarg);
                break;
            case 'R': // Range of scales
                n2 = atoi(optarg);
                break;
            case 't': // Set of scales
                list = 1;
                lscales = strdup(optarg);
                break;
            case 'M': // Input MWT file
                mwtfile = strdup(optarg);
                break;
            case 'N': // No heatmap plots
                noplot = 1;
                break;
            case 'L': // Title for generated plots
                ititle = strdup(optarg);
                break;
            case 'O': // Write a CSV file
                writecsv = 1;
                csvfile = strdup(optarg);
                break;
            case 'm':
                cwtfiles = strdup(optarg);
                break;
            case 'p': // Plot power only
                poweronly = 1;
                break;
            case 's': // Significance level
                siglevel = atof(optarg);
                break;
            case 'F':
                sigpoweronly = 1;
                break;
            default:
                print_usage_exit();
                break;
        }
    }
    
    if (mwtfile)
    {
        float min, max;
        float tr[] = { 0.0, 1.0,  0.0,  0.0,  0.0, 1.0};
        float rl[] = {-0.5, 0.0, 0.17, 0.33, 0.50, 0.67, 0.83, 1.0, 1.7};
        float rr[] = { 0.0, 0.0,  0.0,  0.0,  0.6,  1.0,  1.0, 1.0, 1.0};
        float rg[] = { 0.0, 0.0,  0.0,  1.0,  1.0,  1.0,  0.6, 0.0, 1.0};
        float rb[] = { 0.0, 0.3,  0.8,  1.0,  0.3,  0.0,  0.0, 0.0, 1.0};

        if ((readMWT(mwtfile, &pmwt)) != 0)
        {
            fprintf(stderr, "Failed to read from MWT file: %s.\n", mwtfile);
            destroypsrMWT(&pmwt);
            exit(EXIT_FAILURE);
        }
        free(mwtfile);

        validateLims(lc, lims, pmwt.N, pmwt.bins[1], pmwt.bins[2]);

        for (int i = 1; i <= pmwt.scales[0]; i++)
        {
            minmax(pmwt.mwt[i - 1], pmwt.bw * pmwt.N, &min, &max);

            if (cpgbeg(0, "?", 1, 1) != 1)
                exit(EXIT_FAILURE);

            cpgscr(0, 1, 1, 1);
            cpgscr(1, 0, 0, 0);
            cpgscf(2);
            cpgsch(1.0);
            cpgenv(0, 1, 0, 1, 0, 0);
            cpgctab(rl, rr, rg, rb, 9, 1.0, 0.5);
            cpglab("Bins", "Periods", "");
            cpgsch(1.5);
            cpglab("", "", "Amplitude/Phase");
            cpgsch(1.0);
            cpgimag(pmwt.mwt[i - 1], pmwt.bw, pmwt.N, 1,pmwt.bw - 0, 1, pmwt.N - 0,
                    min / 100, max, tr);
            cpgend();

        }
    } else if (cwtfiles == NULL) {

        if ((readCWT(ifile, &pcwt)) != 0)
        {
            fprintf(stderr, "Failed to read from input CWT file: %s.\n", ifile);
            free(ifile);
            free(pcwt.spec);
            exit(EXIT_FAILURE);
        }

        validateLims(lc, lims, pcwt.N, pcwt.J0, pcwt.J);

        printf("N = %d\tJ = %d\tJ0 = %d\tT = %0.2f\t"
               "W = %d\tw0 = %0.2f\tP = %0.2f\tS = %0.2f\n", 
                pcwt.N, pcwt.J, pcwt.J0, pcwt.T, 
                pcwt.W, pcwt.w0, pcwt.P, pcwt.S);
        sdat = calloc(sizeof(float), 2 * pcwt.N * (pcwt.J - pcwt.J0));

        signif = SQR(pcwt.S) * gsl_cdf_chisq_Qinv(1 - siglevel, 2.0);
        printf("%.3f significance level = %.9f.\n", siglevel, signif);

        for (int i = 0; i < pcwt.N * (pcwt.J - pcwt.J0); i++)
            sdat[i] = (((pcwt.spec[i] * pcwt.spec[i]) >= signif) ? pcwt.spec[i] : 0);

        for (int i =     pcwt.N * (pcwt.J - pcwt.J0); 
                 i < 2 * pcwt.N * (pcwt.J - pcwt.J0); i++)
            sdat[i] = (sdat[i - pcwt.N * (pcwt.J - pcwt.J0)] ? pcwt.spec[i] : 0);

        if (!noplot)
            make_plots(pcwt.spec, sdat, pcwt.N, pcwt.J, pcwt.J0, pcwt.T, 
                       pcwt.w0, pcwt.P, siglevel, signif, ofile, driver, lims, 
                       poweronly, sigpoweronly);

        if (n1)
        {
            if (n2 == 0)
                n2 = n1 + 1;

            int nums = n2 - n1;
            int *scales = calloc(sizeof(int), nums);

            for (int i = 0, j = n1; i < nums && j < n2; i++, j++)
                scales[i] = j;

            make_scale_plots(sdat, pcwt.N, pcwt.J, pcwt.J0, pcwt.T, scales, nums, ofile, driver);
            free(ofile);
        }

        if (list)
        {
            int count = 1;
            char *tok, *lim = ",";
            for (int i = -1; lscales[++i]; count += lscales[i] == lim[0]);
            int *scales = calloc(sizeof(int), count);
    
            int i = -1;
            tok = strtok(lscales, lim);
            while (tok != NULL)
            {
                scales[++i] = atoi(tok);
                tok = strtok(NULL, lim);
            }
        
            make_scale_plots(sdat, pcwt.N, pcwt.J, pcwt.J0, pcwt.T, scales, count, ofile, driver);
        }
        free(pcwt.spec);
        free(sdat);
        free(ofile);
        free(ifile);
        free(driver);
    } else {
        int ncwt = 0;
        cwtfile = strtok(cwtfiles, ",");
        readCWT(cwtfile, &pcwt);
        sdat = calloc(sizeof(float), pcwt.N * (pcwt.J - pcwt.J0));
        validateLims(lc, lims, pcwt.N, pcwt.J0, pcwt.J);
        free(pcwt.spec);
        
        do {
            if ((readCWT(cwtfile, &pcwt)) != 0)
            {
                fprintf(stderr, "Failed to read from input CWT file: %s.\n", ifile);
                free(ifile);
                free(pcwt.spec);
                exit(EXIT_FAILURE);
            }
            ncwt++;

            signif = SQR(pcwt.S) * gsl_cdf_chisq_Qinv(1 - siglevel, 2.0);
            
            for (int i = 0; i < (pcwt.J - pcwt.J0); i++)
                for (int j = 0; j < pcwt.N; j++)
                    if (SQR(pcwt.spec[i * pcwt.N + j]) > signif)
                        sdat[i * pcwt.N + j] += SQR(pcwt.spec[i * pcwt.N + j]) /
                                                    pow(2, 0.1 * (pcwt.J0 + i));
            free(pcwt.spec);
        } while ((cwtfile = strtok(NULL, ",")) != NULL);

        for (int i = 0; i < (pcwt.J - pcwt.J0); i++)
            for (int j = 0; j < pcwt.N; j++)
                sdat[i * pcwt.N + j] /= ncwt;

        plot(sdat, pcwt.spec, pcwt.N, pcwt.J, pcwt.J0, pcwt.T, pcwt.w0, pcwt.P,
                "?",  ititle ? ititle : "Multibin Power", lims, 
                sigpoweronly ? POWER : AMPLITUDE); 
        free(cwtfiles);
        free(sdat);
    }
     
    if (writecsv)
    {
        write_csv(pcwt.spec, pcwt.N, pcwt.J, pcwt.J0, pcwt.T, csvfile);
        free(csvfile);
    }
    
     
    return 0;
}

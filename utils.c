#include <math.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "utils.h"

// read formatted single-pulse file
int readDAT(const char *path, psrDAT *dat)
{
    FILE *p, *f;
    char command[256], header[512], *tmp, buf[256];
    int i = 0;

    if (access(path, F_OK) == -1)
       return 1; 

    sprintf(command, "grep '#' %s | wc | awk '{print $1}'", path);
    if ((p = popen(command, "r")) == NULL)
        return 1;

    fscanf(p, "%d", &dat->Nperiods); 
    fclose(p);

    f = fopen(path, "r");
    fgets(header, sizeof(header) - 1, f);
    strtok(header, " ");
    while ((tmp = strtok(NULL, " ")))
    {
        ++i;
        if (i == 3)
            dat->P     = atof(tmp);
        if (i == 7)
            dat->Nbins = atoi(tmp);
    }

    dat->Ts = dat->P / dat->Nbins;
    dat->N = dat->Nperiods * (dat->Nbins + 1);
    dat->T = dat->Ts * dat->N;

    dat->data = calloc(dat->N, sizeof(double));

    for (int i = 0; i < dat->Nperiods; i++)
    {
        for (int j = 0; j < dat->Nbins; j++)
        {
            fgets(buf, sizeof(buf), f);
            strtok(buf, " ");
            dat->data[i * dat->Nbins + j] = atof(strtok(NULL, " "));
        }
        fgets(buf, sizeof(buf), f);
    }

    fclose(f);

    return 0;
}

int writeCWT(const char *path, psrCWT psrcwt)
{
    FILE *f;

    if (strcmp(path, "-") == 0)
        f = stdout;
    else if ((f = fopen(path, "wb")) == NULL)
        return 1;

    int specsz = 2 * psrcwt.N * (psrcwt.J - psrcwt.J0);
    size_t towrite = 4 * sizeof(int) + (4 + specsz) * sizeof(float);
    size_t written = 0;

    written += sizeof(int)   * fwrite(&psrcwt.N,   sizeof(int),   1,      f);
    written += sizeof(int)   * fwrite(&psrcwt.J,   sizeof(int),   1,      f);
    written += sizeof(int)   * fwrite(&psrcwt.J0,  sizeof(int),   1,      f);
    written += sizeof(float) * fwrite(&psrcwt.T,   sizeof(float), 1,      f);
    written += sizeof(int)   * fwrite(&psrcwt.W,   sizeof(int),   1,      f);
    written += sizeof(float) * fwrite(&psrcwt.w0,  sizeof(float), 1,      f);
    written += sizeof(float) * fwrite(&psrcwt.P,   sizeof(float), 1,      f);
    written += sizeof(float) * fwrite(&psrcwt.S,   sizeof(float), 1,      f);
    written += sizeof(float) * fwrite(psrcwt.spec, sizeof(float), specsz, f);

    fclose(f);

    if (towrite - written != 0)
        return 1;

    return 0;
}

int readCWT(const char *path, psrCWT *psrcwt)
{
    FILE *f;

    if (strcmp(path, "-") == 0)
        f = stdin;
    else if ((f = fopen(path, "rb")) == NULL)
        return 1;

    size_t toread, specsz, read = 0;

    read += sizeof(int)   * fread(&psrcwt->N,   sizeof(int),    1,      f);
    read += sizeof(int)   * fread(&psrcwt->J,   sizeof(int),    1,      f);
    read += sizeof(int)   * fread(&psrcwt->J0,  sizeof(int),    1,      f);
    read += sizeof(float) * fread(&psrcwt->T,   sizeof(float),  1,      f);
    read += sizeof(int)   * fread(&psrcwt->W,   sizeof(int),    1,      f);
    read += sizeof(float) * fread(&psrcwt->w0,  sizeof(float),  1,      f);
    read += sizeof(float) * fread(&psrcwt->P,   sizeof(float),  1,      f);
    read += sizeof(float) * fread(&psrcwt->S,   sizeof(float),  1,      f);

    specsz  = 2 * psrcwt->N * (psrcwt->J - psrcwt->J0);
    psrcwt->spec = calloc(specsz, sizeof(float));
    read += sizeof(float) * fread(psrcwt->spec, sizeof(float),  specsz, f);
    
    toread = 4 * sizeof(int) + (4 + specsz) * sizeof(float);

    fclose(f);

    if (toread - read != 0)
        return 1;

    return 0;
}

int writeMWT(const char *path, psrMWT pmwt)
{
    FILE *f;

    if (strcmp(path, "-") == 0)
        f = stdout;
    else if ((f = fopen(path, "wb")) == NULL)
        return 1;

    size_t towrite = (6 + 2 * pmwt.scales[0])                                  * sizeof(int) + 
                     (2 + pmwt.bw + (2 * pmwt.size + pmwt.N) * pmwt.scales[0]) * sizeof(float);
    size_t written = 0;

    written += sizeof(int)    * fwrite(&pmwt.scales[0], sizeof(int),   1,         f);
    written += sizeof(int)    * fwrite(&pmwt.N,         sizeof(int),   1,         f);
    written += sizeof(int)    * fwrite(&pmwt.Nb,        sizeof(int),   1,         f);
    written += sizeof(int)    * fwrite(&pmwt.bins[1],   sizeof(int),   2,         f);
    written += sizeof(float)  * fwrite(&pmwt.T,         sizeof(float), 1,         f);
    written += sizeof(int)    * fwrite(&pmwt.W,         sizeof(int),   1,         f);
    written += sizeof(float)  * fwrite(&pmwt.P,         sizeof(float), 1,         f);
    written += sizeof(float)  * fwrite( pmwt.S,         sizeof(float), pmwt.bw,   f);


    for (int i = 1; i <= pmwt.scales[0]; i++)
    {
        written += sizeof(int)   * fwrite(&pmwt.scales[i],     sizeof(int),   1,             f);
        written += sizeof(int)   * fwrite(&pmwt.size,          sizeof(int),   1,             f);
        written += sizeof(float) * fwrite( pmwt.mwt[i - 1],    sizeof(float), 2 * pmwt.size, f);
        written += sizeof(float) * fwrite( pmwt.powsums[i - 1],sizeof(float), pmwt.N,        f);
    }

    fclose(f);

    if (towrite - written != 0)
        return 1;

    return 0;
}

int  readMWT(const char *path, psrMWT *pmwt)
{
    FILE *f;

    if (strcmp(path, "-") == 0)
        f = stdin;
    else if ((f = fopen(path, "rb")) == NULL)
        return 1;
    
    size_t toread, read = 0;
    int nScales;
    pmwt->bins = calloc(3, sizeof(int));

    read += sizeof(int)    * fread(&nScales,          sizeof(int),   1,        f);
    read += sizeof(int)    * fread(&pmwt->N,          sizeof(int),   1,        f);
    read += sizeof(int)    * fread(&pmwt->Nb,         sizeof(int),   1,        f);
    read += sizeof(int)    * fread(&pmwt->bins[1],    sizeof(int),   2,        f);
    read += sizeof(float)  * fread(&pmwt->T,          sizeof(float), 1,        f);
    read += sizeof(int)    * fread(&pmwt->W,          sizeof(int),   1,        f);
    read += sizeof(float)  * fread(&pmwt->P,          sizeof(float), 1,        f); 
    
    pmwt->mwt     = malloc(nScales * sizeof(float*));
    pmwt->powsums = malloc(nScales * sizeof(float*));
    pmwt->bw = pmwt->bins[2] - pmwt->bins[1];
    pmwt->S = calloc(pmwt->bw, sizeof(float));
    pmwt->scales = calloc(nScales + 1, sizeof(int));
    pmwt->scales[0] = nScales;
    read += sizeof(float)  * fread( pmwt->S,          sizeof(float), pmwt->bw, f);

    for (int i = 1; i <= pmwt->scales[0]; i++)
    {
        read += sizeof(int)   * fread(&pmwt->scales[i],     sizeof(int),   1,              f);
        read += sizeof(int)   * fread(&pmwt->size,          sizeof(int),   1,              f);
        pmwt->mwt[i - 1]     = calloc(2 * pmwt->size, sizeof(float));
        pmwt->powsums[i - 1] = calloc(       pmwt->N, sizeof(float));
        read += sizeof(float) * fread( pmwt->mwt[i - 1],    sizeof(float), 2 * pmwt->size, f);
        read += sizeof(float) * fread( pmwt->powsums[i - 1],sizeof(float), pmwt->N,        f); 
    }

    fclose(f);

    toread = (6 + 2 * pmwt->scales[0])                                     * sizeof(int) + 
             (2 + pmwt->bw + (2 * pmwt->size + pmwt->N) * pmwt->scales[0]) * sizeof(float);

    if (toread - read != 0)
        return 1;

    return 0;
}

void destroypsrMWT(psrMWT *pmwt)
{
    for (int i = 0; i < pmwt->scales[0]; i++)
    {
        free(pmwt->mwt[i]);
        free(pmwt->powsums[i]);
    }

    free(pmwt->mwt);
    free(pmwt->powsums);
    free(pmwt->S);
    free(pmwt->scales);
    free(pmwt->bins);
}

double* linspace(const double x1, const double x2, const int n)
{
    double *space = malloc(n * sizeof(double));
    double incr = ((x2 - x1) / (double) (n - 1));

    for (int i = 0; i < n; i++)
        space[i] = x1 + i * incr;

    return space;
}

float* linspace_f(const float x1, const float x2, const int n)
{
    float *space = malloc(n * sizeof(float));
    float incr = ((x2 - x1) / (float) (n - 1));

    for (int i = 0; i < n; i++)
        space[i] = x1 + i * incr;

    return space;
}

int* HoughTransform(int* image, int M, int N, int nAngles)
{
    int size = (int) ceil(sqrt(M * M + N * N));
    int *transform = calloc(size * nAngles, sizeof(int));
    double *angles = linspace(-pi / 2, pi / 2, nAngles);
    double p;

    for (int i = 0; i < M; i++)
        for (int j = 0; j < N; j++)
            if (image[i * N + j])
                for (int k = 0; k < nAngles; k++)
                {
                    if ((p = round(j * cos(angles[k]) + i * sin(angles[k]))) < 0)
                        continue;
                    else
                        transform[(int) p * nAngles + k] += 1;
                }
    free(angles);
    return transform;
}

int locMinInd(int *data, size_t N, size_t stride, int *ind, int thres)
{
    int n = 0;

    for (int i = 1; i < N - 1; i++)
        if (data[stride * (i - 1)] < data[stride * i] && 
            data[stride * (i + 1)] < data[stride * i] && 
            data[stride * i] >= thres)
            ind[n++] = i;

//    ind = realloc(ind, n * sizeof(int));

    return n;
}

float distlp(float l[2], int p[2])
{
    return fabsf((float) p[0] - (float) p[1] / l[0] + l[1] / l[0]) / 
           sqrtf(1 + (1 /  l[0]) * (1 / l[0]));
}

float minmax(float *data, const size_t N, float *min, float *max)
{
    *min = data[0]; *max = data[0];

    for (int i = 0; i < N; i++)
        if (data[i] > *max)
            *max = data[i];
        else if (data[i] < *min)
            *min = data[i];

    return max - min;
}

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>

#include "cwt.h"
#include "utils.h"

void print_usage_exit(void)
{
    fprintf(stderr, "USAGE: cwt1    -i / -I <input file/single pulse data file>\n"
                    "               -N <number of samples to read (not required with -I)>\n"
                    "               -J <highest scale>\n"
                    "               -K <initial scale>\n"
                    "               -T <total time duration (not required with -I)\n"
                    "               -o <output CWT file>\n"
                    "               -b <bins to read from single pulse file; comma-separated list with no spaces>\n"
                    "               -P <specify period for unformatted text files; wavreader will display frequency\n"
                    "                   in Hertz if this is not specified.>"
                    "               -U <no options; unwraps phases along translation for each scale>\n"
                    "               -W <0 for Morlet, 1 for Mexican hat; default=0>\n"
                    "               -w <order/centre frequency of the wavelet; default=2pi>\n"
                    "               -v <no options; verbose output: timing information and wavelet is dumped to /tmp.>\n");
    exit(1);
}

int main(int argc, char **argv)
{
    int N = 0, J = 0, J0 = 0, W = -1, binNo = 0;
    float T = 0, S = 0, w0 = 2 * pi, P = -1;
    char *ifile = NULL, *ofile = NULL;
    FILE *fp1 = NULL;
    int opt = 0, read = 0, write = 0, unwrap = 0, verbose = 0, rdat = 0, bin = 0;
    float *spec = NULL;
    double *y = NULL;

    psrDAT dat = {NULL, 0, 0, 0, 0, 0, 0};

    struct timespec startm, stopm, startc, stopc;

    if (argc < 4)
        print_usage_exit();

    extern char *optarg;
    while ((opt = getopt(argc, argv, "I:N:J:K:T:i:o:UP:b:W:w:v")) != -1)
    {
        switch (opt)
        {
            case 'I':         // For single pulse data files.
                if (read == 1 || N != 0 || T != 0)
                {
                    fprintf(stderr, "Can't use -I with -i, -N, -T.\n");
                    exit(1);
                }
                rdat = 1;
                ifile = strdup(optarg);
                break;
            case 'N':         // Number of samples to read.
                N = atoi(optarg);
                break;
            case 'J':         // Starting scale.
                J = atoi(optarg);
                break;
            case 'K':         // Final scale.
                J0 = atoi(optarg);
                break;
            case 'T':         // Total time duration.
                if (rdat == 1)
                {
                    fprintf(stderr, "Can't use -T with -I.\n");
                    exit(1);
                }
                T = atof(optarg);
                break;
            case 'P':         // Specify period for unformatted input files (-i)
                P = atof(optarg);
                break;
            case 'i':         // For unformatted time series files.
                if (rdat == 1)
                {
                    fprintf(stderr, "Can't use -i with -I.\n");
                    exit(1);
                }
                read = 1;
                ifile = strdup(optarg);
                break;
            case 'o':        // Output cwt file.
                write = 1;
                ofile = strdup(optarg);
                break;
            case 'b':        // Specify bins to read from single pulse file.
                bin = 1;
                binNo = atoi(optarg);
                break;
            case 'U':        // Unwrap phases.
                unwrap = 1;
                break;
            case 'W':        // Specify wavelet; 0 = Morlet, 1 = Mexican hat.
                W  = atoi(optarg);
                break;
            case 'w':        // Order / centre frequency of the wavelet
                w0 = atof(optarg);
                break;
            case 'v':        // More output, timing information etc.
                verbose = 1;
                break;
            default:
                print_usage_exit();
                break;

        }
    }

    if (J0 > J)
    {
        fprintf(stderr, "Initial scale K cannot be greater than highest scale J!");
        exit(1);
    }

    if (W < 0 || W > 1)
    {
        fprintf(stderr, "Wavelet not specified or invalid, using Morlet.\n");
        W = 0;
    }

    if (w0 == 2 * pi)
        fprintf(stderr, "Wavelet frequency/order not specified, using %.2f.\n", w0);

    if (read)
    {
        fp1 = fopen(ifile, "r");
        y = calloc(N, sizeof(double));
        for (int i = 0; i < N; i++)
            fscanf(fp1, "%lf", &y[i]);
        fclose(fp1);
        free(ifile);
    } else if (rdat)
    {
        if (readDAT(ifile, &dat))
        {
            fprintf(stderr, "Failed to read input file: %s", ifile);
            exit(1);
        }

        if (bin)
        {
            printf("Using bin: %d.\n", binNo);

            N = N ? N : dat.Nperiods;
            y = calloc(N, sizeof(double));

            for (int i = 0; i < N; i++)
                y[i] = dat.data[i * dat.Nbins + binNo];

        } else {
            N = dat.N;
            y = calloc(N, sizeof(double));
            memcpy(y, dat.data, N);
        }

        T = (float) N / (float) dat.Nperiods * dat.T;
        free(dat.data);
        free(ifile);
    } else {
        double *x = linspace(0, T, N);
        y = calloc(N, sizeof(double));
        for (int i = 0; i < N; i++)
            y[i] = sin(2 * pi * 1 * exp(x[i]));
/*        for (int i = 0; i < N / 2; i++)
            y[i] = 1.0 * cos(2 * pi * 0.4 * x[i]);
        for (int i = N / 2; i < N; i++)
            y[i] = 1.0 * cos(2 * pi * 0.8 * x[i]);
*/       free(x);
    }

    S = (float) gsl_stats_sd(y, 1, N);

    fftw_complex *(*wavelets[2]) (const int N, const int J, const int J0, const float w0);
    wavelets[0] = morlet_fft;
    wavelets[1] = mexican_fft;

    clock_gettime(CLOCK_MONOTONIC, &startm);
    fftw_complex *m = (*wavelets[W])(N, J, J0, w0);
    clock_gettime(CLOCK_MONOTONIC, &stopm);

    clock_gettime(CLOCK_MONOTONIC, &startc);
    spec = cwt(y, m, N, J, J0, unwrap);
    clock_gettime(CLOCK_MONOTONIC, &stopc);

    if (write)
    {
        psrCWT pcwt = {spec, w0, rdat ? dat.P : P, N, J, J0, W, T, S};
        if (writeCWT(ofile, pcwt) != 0)
        {
            fprintf(stderr, "Failed to write to %s.", ofile);
            remove(ofile);
            exit(1);
        }
        free(ofile);
    }
    
    if (verbose)
    {
        fprintf(stderr, "GEN took:\t%lu\nCWT took:\t%lu\n", 
            1000000000 * (stopm.tv_sec - startm.tv_sec) + (stopm.tv_nsec - startm.tv_nsec), 
            1000000000 * (stopc.tv_sec - startc.tv_sec) + (stopc.tv_nsec - startc.tv_nsec));


        FILE *out = fopen("/tmp/wavelet.dat", "w");
    
        for (int i = 0; i < J - J0; i++)
            for (int j = 0; j < N; j++)
                fprintf(out, "%.4f%s", FFTABS(m[i * N + j]), CSVFMT(j, N));

        fclose(out);
    }

    free(spec);
    free(y);
    fftw_free(m);

    return 0;
}

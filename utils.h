#include <math.h>
#include <stdlib.h>

#ifndef __UTILS_H
#define __UTILS_H

static const double pi = 3.14159265358979323846;

typedef struct psrDAT
{
    double *data;
    double P, T, Ts;
    int Nbins, Nperiods, N;
} psrDAT;

typedef struct psrCWT
{
    float *spec, w0, P;
    int N, J, J0, W;
    float T, S; 
} psrCWT;

typedef struct psrMWT
{
    float **mwt, **powsums, P;
    int N, Nb, *bins, *scales, W, size, bw;
    float T, *S;
} psrMWT;

double* linspace  (const double x1, const double x2, const int n);
float * linspace_f(const float  x1, const float  x2, const int n);

int readDAT(const char *path, psrDAT *dat);

int writeCWT(const char *path, psrCWT  pcwt);
int  readCWT(const char *path, psrCWT *pcwt);

int writeMWT(const char *path, psrMWT  pmwt);
int  readMWT(const char *path, psrMWT *pmwt);
void destroypsrMWT(psrMWT *pmwt);

int* HoughTransform(int* image, int M, int N, int nAngles);

int locMinInd(int *data, size_t N, size_t stride, int *ind, int thres);
float distlp(float l[2], int p[2]);
float minmax(float *data, const size_t N, float *min, float *max);

static inline float ftos(const float wCoef, const int N, const float T, const float f)
{
    return 10 * log2f((((float) N) * wCoef) / (T * f));
}

static inline float stof(const float wCoef, const int N, const float T, const float s)
{
    return wCoef / (powf(2, s / 10) * T / ((float) N));
}

#endif // __UTILS_H

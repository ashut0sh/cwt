#include <stdio.h>
#include <stdlib.h>
#include <cpgplot.h>
#include <gsl/gsl_histogram.h>
#include <gsl/gsl_statistics.h>
#include <gsl/gsl_statistics_double.h>

int compare_floats(const void *x, const void *y)
{
    const float *a = (float*) x;
    const float *b = (float*) y;
    if (*a > *b)
       return 1;
    else if (*a < *b)
       return -1;
    else
       return 0;
}

void plot_histogram(const gsl_histogram *h)
{
    float min, max;
    float *x = calloc(h->n, sizeof(float));
    float *y = calloc(h->n, sizeof(float));

    if (cpgbeg(0, "?", 1, 1) != 1)
        exit(EXIT_FAILURE);

    for (int i = 0; i < h->n; i++)
        x[i] = (float) h->range[i], y[i] = (float) h->bin[i];

    gsl_stats_float_minmax(&min, &max, y, 1, h->n);

    cpgscr(0, 1, 1, 1);
    cpgscr(1, 0, 0, 0);
    cpgscf(2);
    cpgsch(1.5);
    cpgenv(x[0], x[h->n - 1], 0.9 * min, 1.1 * max, 0, 0);
    cpglab("Slopes (ms/period)", "Count", "");
    cpgsch(2.0);
    cpglab("", "", "Distribution of Slopes");
    cpgsch(1.0);
    cpgbin(h->n, x, y, 0);
   
    cpgend();

    free(x);
    free(y);
}

int main(int argc, char *argv[])
{
    size_t sz = 1 << 16, nSlopes = 0;
    const int nBins = 100;
    float *slopes = calloc(sz, sizeof(float));
    float min, mdn, max;
    char buf[512];
    FILE *f;

    for (int i = 1; i < argc; i++)
    {
        if ((f = fopen(argv[i], "r+")) == NULL)
        {
            perror("Failed to open input file");
            exit(EXIT_FAILURE);
        }

        // Skip 3 lines of header
        fscanf(f, "%*[^\n]\n%*[^\n]\n%*[^\n]\n");
        
        while (fgets(buf, sizeof(buf), f))
            sscanf(buf, "%*f\t\t%f\t\t%*f\t\t%*f\t%*d\n", &slopes[nSlopes++]);

        if (2 * nSlopes >= sz)
            slopes = realloc(slopes, (sz <<= 2) * sizeof(float));

        fclose(f);
    }

    slopes = realloc(slopes, nSlopes * sizeof(float)); 

    qsort(slopes, nSlopes, sizeof(float), compare_floats);
    mdn = gsl_stats_float_median_from_sorted_data(slopes, 1, nSlopes);
    min = gsl_stats_float_quantile_from_sorted_data(slopes, 1, nSlopes, 
                                            mdn < 0 ? 0.001 : 0.000) * 0.9;
    max = gsl_stats_float_quantile_from_sorted_data(slopes, 1, nSlopes, 
                                            mdn < 0 ? 1.000 : 0.999) * 1.1;
    gsl_histogram *h = gsl_histogram_alloc(nBins);
    gsl_histogram_set_ranges_uniform(h, min, max);

    for (int i = 0; i < nSlopes; i++)
        gsl_histogram_increment(h, slopes[i]);

    plot_histogram(h);

    gsl_histogram_free(h);
    free(slopes);

    return 0;
}

#include <math.h>
#include <time.h>
#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <gsl/gsl_statistics_double.h>

#include "utils.h"
#include "cwt.h"

#define MTHREADS 4

// Threaded multiplication for convolution
void* runner_mul(void *arg)
{
    mulrunner_args *args = (mulrunner_args*) arg;
    
    for (int i = args->j1; i < args->j2; i++)
    {
        for (int j = 0; j < args->N; j++)
        {
            args->in[(i - args->J0) * args->N + j][REAL] = 
                args->out[j][REAL] * args->B[(i - args->J0) * args->N + j][REAL] 
              - args->out[j][IMAG] * args->B[(i - args->J0) * args->N + j][IMAG];
            args->in[(i - args->J0) * args->N + j][IMAG] = 
                args->out[j][REAL] * args->B[(i - args->J0) * args->N + j][IMAG] 
              + args->out[j][IMAG] * args->B[(i - args->J0) * args->N + j][REAL];
        }
    }
    pthread_exit(NULL);
}

// Generate Morlet wavelet
void* runner_m(void *arg)
{
    mrunner_args *args = (mrunner_args*) arg;

    for (int i = args->j1; i < args->j2; i++)
        for (int j = 0; j < args->M; j++)
            args->f_morlet[(i - args->J0) * args->N + j][REAL] = sqrt(2 * pi * pow(2, 0.1 * i)) 
                * exp(-SQR(pow(2, 0.1 * i) * j / args->N - args->w0) / 2.0);
    
    pthread_exit(NULL);
}


// Itoh's algorithm.
void phase_unwrap(float *w, int N) 
{
    float *diff        = calloc(sizeof(float), N - 1);

    for (int i = 0; i < N - 1; i++)
        diff[i] = atanf(tanf(w[i + 1] - w[i]));
    
    for (int i = 1; i < N; i++)
        w[i] = w[i - 1] + diff[i - 1];

    free(diff);
}

// Threaded function to generate Fourier transform of Morlet wavelet 
// at all the required scales.
fftw_complex* morlet_fft(const int N, const int J, const int J0, const float w0)
{
    const int M = N / 2;

    fftw_complex *morlet_fft = fftw_malloc(sizeof(fftw_complex) * (N * (J - J0)));
    memset(morlet_fft, 0, (N * (J - J0)) * sizeof(fftw_complex));

    mrunner_args    arg[MTHREADS];
    pthread_t         t[MTHREADS];
    pthread_attr_t attr[MTHREADS];

    for (int i = 0; i < MTHREADS; i++)
    {
        pthread_attr_init(&attr[i]);
        arg[i] = (mrunner_args) {morlet_fft, w0, N, M, J, J0, J0 + i * (J - J0) / MTHREADS, 
                                                J0 + (i + 1) * (J - J0) / MTHREADS};
        pthread_create(&t[i], &attr[i], runner_m, (void *) &arg[i]);
    }

    for (int i = 0; i < MTHREADS; i++)
        pthread_join(t[i], NULL);
     
    return morlet_fft;
}

// Function to generate Fourier transform of Mexican hat wavelet 
// at all the required scales.
// TODO: threading
fftw_complex* mexican_fft(const int N, const int J, const int J0, const float w0)
{
    const int M = N / 2;

    fftw_complex *mexican_fft = fftw_malloc(sizeof(fftw_complex) * (N * (J - J0)));
    memset(mexican_fft, 0, (N * (J - J0)) * sizeof(fftw_complex));

    for (int i = 0; i < (J - J0); i++)
        for (int j = 0; j < M; j++)
            mexican_fft[i * N + j][REAL] = 2 / sqrt(3.0) * pow(pi, -0.25) 
                * SQR(pow(2, 0.1 * (i + J0)) * ((double) j) / M) 
                * exp(-0.5 * SQR(pow(2, 0.1 * (i + J0)) * ((double) j) / M));
    
    return mexican_fft;
}

// Performs wavelet transform using provided wavelet B. Implemented using
// convolution as described in Torrence and Compo (1998). plans[0], the first
// FFTW plan takes FFT of the provided time series. For efficiency in threaded
// implementation, the inverse FFT is implemented in plans[1] as 2D N x (J - J0)
// transform, performed along the translation axis. Saves the wavelet transform
// data in a 2N x (J - J0) array, amplitudes followed by phases.
float* cwt(double *in, fftw_complex *B, const int N, const int J, const int J0,
                const int unwrap)
{
    float *cwt = calloc(sizeof(float), 2 * N * (J - J0));
    double *ind  = calloc(sizeof(double), N);

    char filename[128] = { 0 };
    char *cwtdir = getenv("CWTDIR");
    sprintf(filename, "%s%swisdom/%d_%d.fftw", 
                       cwtdir != NULL ? cwtdir : "",
                       cwtdir != NULL ? "/"    : "",
                       N, (J - J0));

    fftw_init_threads();
    fftw_plan_with_nthreads(MTHREADS);

    if (fftw_import_wisdom_from_filename(filename) != 1)
    {
        perror("Failed to load wisdom");
//        exit(1);
    }

    int rank = 1, idist = N, odist = N, istride = 1, ostride = 1;
    int howmany = (J - J0), n[] = {N}, *inembed = n, *onembed = n;

    fftw_complex *out1 = fftw_alloc_complex(N);
    fftw_complex *out2 = fftw_alloc_complex(N * (J - J0));
    memset(out1, 0, N * sizeof(fftw_complex));
    memset(out2, 0, (N * (J - J0)) * sizeof(fftw_complex));
    fftw_complex *inc  = fftw_alloc_complex(N * (J - J0));

    fftw_plan plans[2];
    plans[0] = fftw_plan_dft_r2c_1d(N, in,  out1,                FFTW_ESTIMATE);
    plans[1] = fftw_plan_many_dft  (rank, n, howmany, inc, inembed, istride, idist,
                                                     out2, onembed, ostride, odist,
                                                     FFTW_BACKWARD, FFTW_ESTIMATE);

    if (plans[0] == NULL || plans[1] == NULL)
    {
        printf("Failed to create plan. Exiting...");
        exit(1);
    }

    fftw_execute(plans[0]);

    mulrunner_args      marg[MTHREADS];
    pthread_t           tids[MTHREADS];
    pthread_attr_t      attr[MTHREADS];

    for (int i = 0; i < MTHREADS; i++)
    {
        pthread_attr_init(&attr[i]);
        marg[i] = (mulrunner_args) {inc, out1, B, N, J, J0, J0 + i * (J - J0) / MTHREADS, 
                                                      J0 + (i + 1) * (J - J0) / MTHREADS};
        pthread_create(&tids[i], &attr[i], runner_mul, (void *) &marg[i]);
    }

    for (int i = 0; i < MTHREADS; i++)
        pthread_join(tids[i], NULL);

    fftw_execute(plans[1]);

    for (int i = 0; i < N * (J - J0); i++)
        cwt[i]                 = (float) FFTABS(out2[i]) / ((float) N);

    for (int i = 0; i < N * (J - J0); i++)
        cwt[N * (J - J0) + i]  = (float) FFTTAN(out2[i]);

    if (unwrap)
        for (int i = N * (J - J0); i < 2 * N * (J - J0); i += N)
            phase_unwrap(&cwt[i], N);

    fftw_export_wisdom_to_filename(filename);
    fftw_destroy_plan(plans[0]);
    fftw_destroy_plan(plans[1]);
    fftw_free(inc);
    fftw_free(out1);
    fftw_free(out2);
    fftw_cleanup_threads();
    fftw_cleanup();
    free(ind);
    
    return cwt;
}
